package com.massoudafrashteh.code.spring.boot.dans.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.massoudafrashteh.code.spring.boot.dans.domain.User;
import com.massoudafrashteh.code.spring.boot.dans.repository.UserRepository;
import com.massoudafrashteh.code.spring.boot.dans.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	public void save(final User user) {
		userRepository.save(user);
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public User findById(final long id) {
		return userRepository.findOne(id);
	}

	public void deleteById(final long id) {
		userRepository.delete(id);
	}
}
