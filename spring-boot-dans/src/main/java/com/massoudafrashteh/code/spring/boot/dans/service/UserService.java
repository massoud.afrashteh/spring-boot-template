package com.massoudafrashteh.code.spring.boot.dans.service;

import java.util.List;

import com.massoudafrashteh.code.spring.boot.dans.domain.User;

public interface UserService {
	public void save(User user);

	public List<User> findAll();

	public User findById(long id);

	public void deleteById(long id);
}
