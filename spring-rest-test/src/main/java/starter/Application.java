package starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// @EnableJpaRepositories("com.massoudafrashteh.code.spring.boot.dans.repository")
public class Application {
	public static void main(final String[] args) {
		SpringApplication.run(Config.class, args);
	}
}