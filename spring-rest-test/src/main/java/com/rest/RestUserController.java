package com.rest;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.massoudafrashteh.code.spring.boot.dans.domain.User;
import com.massoudafrashteh.code.spring.boot.dans.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class RestUserController {

	@Autowired
	private UserRepository userService;

	@RequestMapping("")
	public List<User> findAll() {
		return userService.findAll();
	}

	@RequestMapping("/add")
	public void add() {
		final User user = new User(LocalDateTime.now().toString());
		System.out.println("new user is " + user);
		userService.save(user);
	}

	@RequestMapping("/ping")
	public String ping() {
		return "pong";
	}

}
